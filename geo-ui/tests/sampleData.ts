import {OfficeResponse} from '../src/types/OfficeResponse';

export const sampleSortedResponse: OfficeResponse[] = [
  {
    urlName: 'blue-square-360',
    organization: 'Blue Square 360',
    address: 'St Saviours Wharf, London SE1 2BE',
    coordinates: [-0.0713608999999451, 51.5014767],
    location: 'London, UK',
    services:
      "Blue Square 360 provides a professionally managed service covering all areas of a 360Â° Feedback initiative. We're experienced in supporting projects of all sizes, and always deliver a personal service that provides the level of support you need to ensure your 360 initiative delivers results for the business.",
    distance: '5.14',
  },
  {
    urlName: 'gallus-consulting',
    organization: 'Gallus Consulting',
    address: 'No1 Royal Exchange, London, EC3V 3DG',
    coordinates: [-0.08757919999993646, 51.5136102],
    location: 'London',
    services:
      "We're strategy consultants with a difference - we work with organisations and their leaders to take them from strategy to reality. In our work with leaders we often use 360-degree feedback to identify capability gaps, improve self-awareness, and develop strategic and cultural alignment. Our aim is for believe-able leaders to emerge with the drive, capability and cultural fit to take strategy to reality.",
    distance: '3.81',
  },
  {
    urlName: 'gallus-consulting',
    organization: 'Gallus Consulting',
    address: 'Newton House, Northampton Science Park, Moulton Park, Kings Park Road, Northampton, NN3 6LG',
    coordinates: [-0.877935999999977, 52.277409],
    location: 'Northampton',
    services:
      "We're strategy consultants with a difference - we work with organisations and their leaders to take them from strategy to reality. In our work with leaders we often use 360-degree feedback to identify capability gaps, improve self-awareness, and develop strategic and cultural alignment. Our aim is for believe-able leaders to emerge with the drive, capability and cultural fit to take strategy to reality.",
    distance: '98.71',
  },
  {
    urlName: 'inspire-ignite',
    organization: 'Inspire - Ignite',
    address: '29 Warren Court, Hampton Hargate, Peterborough, PE7 8HA',
    coordinates: [-0.2713853000000199, 52.5381398],
    location: 'Peterborough, UK',
    services:
      "'Inspire - Ignite' is built on the philosophy that people perform better when they can be themselves. We work with individuals and organisations to be their best, achieve ambitions and be a success. We use Clarity4D and the concept of colour and 360 to build personal awareness and action plans.",
    distance: '114.17',
  },
  {
    urlName: 'spring-development',
    organization: 'Spring Development',
    address: 'Banbury, Oxfordshire',
    coordinates: [-1.3397750000000315, 52.0629009],
    location: 'Banbury, Oxfordshire',
    services:
      "We provide training, coaching and consultancy to ensure that 360 feedback is delivered positively and constructively, maximising personal development. We can train your people to carry out effective feedback meetings or, if you would benefit from having external, impartial facilitators, we can come and do them for you. We're always happy to have an initial confidential discussion to explore how we can help you.",
    distance: '102.47',
  },
];
