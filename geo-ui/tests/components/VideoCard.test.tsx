import React from 'react';
import '@testing-library/jest-dom';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';
import {CardList} from '../../src/components/CardList';
import {sampleSortedResponse} from '../sampleData';
import {Card} from '../../src/components/Card';
import {OfficeResponse} from '../../src/types/OfficeResponse';
import {Form} from '../../src/components/Form';

let sampleData: OfficeResponse;
beforeAll(() => {
  sampleData = sampleSortedResponse[0];
});

test('clicking the submit button it should display the cards', async () => {
  render(<CardList offices={sampleSortedResponse} />);

  expect(screen.getAllByRole('location-card').length).toEqual(5);
});

test('organization, address, services date should be appeared', async () => {
  render(<Card company={sampleData} />);

  expect(await screen.findByText(sampleData.organization)).toBeInTheDocument();
  expect(await screen.findByText(sampleData.address)).toBeInTheDocument();
  expect(await screen.findByText(sampleData.services)).toBeInTheDocument();
});

test('form', async () => {
  const onSubmitFn = jest.fn();
  render(<Form onSubmit={onSubmitFn} />);

  userEvent.click(await screen.findByText('Search'));
  expect(onSubmitFn).toBeCalledTimes(2);
});
