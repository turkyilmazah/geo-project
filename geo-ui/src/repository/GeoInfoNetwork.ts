import {get} from './network';

const baseUrl = process.env.REACT_APP_GEO_API_URL;
const url = {
  officeInfo: baseUrl + '/office-info',
};

export class GeoInfoNetwork {
  getAllWithCircle(lat: number, lng: number, distance: number) {
    return get(`${url.officeInfo}/${lat}/${lng}/${distance}`);
  }
}

export const geoInfoNetwork = new GeoInfoNetwork();
