import React, {useState} from 'react';
import {Form} from '../components/Form';
import {CardList} from '../components/CardList';
import {OfficeResponse} from '../types/OfficeResponse';
import {geoInfoNetwork} from '../repository/GeoInfoNetwork';

export function OfficeDestinationBar(props: {onDataDisplayed: (offices: OfficeResponse[]) => void}) {
  const [offices, setOffices] = useState<OfficeResponse[]>([]);
  const getOffices = (distance: number) => {
    geoInfoNetwork.getAllWithCircle(51.5144636, -0.142571, distance).then(response => {
      console.log('response', response);
      setOffices(response);
      props.onDataDisplayed(response);
    });
  };

  return (
    <div style={{paddingTop: 32}}>
      <div style={{height: 48, paddingLeft: 16, paddingRight: 16}}>
        <Form onSubmit={getOffices} />
      </div>
      <div
        style={{
          height: 'calc(100vh - 64px - 32px)',
          paddingTop: 12,
          overflow: 'auto',
          paddingLeft: 16,
          paddingRight: 16,
        }}>
        <CardList offices={offices} />
      </div>
    </div>
  );
}
