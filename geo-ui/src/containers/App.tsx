import React, {useState} from 'react';
import {Map} from '../components/Map';
import {OfficeResponse} from '../types/OfficeResponse';
import {OfficeDestinationBar} from './OfficeDestinationBar';
import '../assets/styles/app.css';
import 'mapbox-gl/dist/mapbox-gl.css';

export const App = () => {
  const [info, setInfo] = useState<OfficeResponse[]>([]);
  const onDataDisplayed = (offices: OfficeResponse[]) => {
    setInfo(offices);
  };
  return (
    <div className="">
      <div className="d-flex">
        <div style={{height: '100vh', width: 360}}>
          <OfficeDestinationBar onDataDisplayed={onDataDisplayed} />
        </div>
        <div className="flex-grow-1">
          <Map offices={info} />
        </div>
      </div>
    </div>
  );
};
