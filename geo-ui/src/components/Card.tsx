import React, {useState} from 'react';
import {OfficeResponse} from '../types/OfficeResponse';

export const Card = (props: {company: OfficeResponse; onPressedLocation?: () => void}) => {
  const [showMore, setShowMore] = useState<boolean>(false);
  const {company} = props;

  return (
    <div role="location-card" className="card mb-3">
      <div className="card-body">
        <h5 className="card-title">{company.organization}</h5>
        <h6 className="card-subtitle mb-2 text-muted">{company.address}</h6>
        <p className={`card-text ${!showMore && 'text-truncate'}`}>{company.services}</p>
        <a href="javascript: void(0)" onClick={() => setShowMore(prevState => !prevState)} className="card-link">
          {showMore ? 'Less' : 'More'}
        </a>
      </div>
    </div>
  );
};
