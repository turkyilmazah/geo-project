import React, {useState} from 'react';

export function Form(props: {onSubmit: (distance: number) => void}) {
  const [text, setText] = useState<string>('');
  const handleSubmit = async (event: any) => {
    event.preventDefault();
    let distance: number = +text;
    if (isNaN(distance)) {
    }
    props.onSubmit(distance);
  };
  return (
    <div className="mb-3">
      <form onSubmit={handleSubmit}>
        <div className="form-row align-items-center">
          <div className="col-auto">
            <label className="sr-only" htmlFor="inlineFormInput">
              Destination
            </label>
            <input
              type="text"
              className="form-control"
              placeholder="Distance"
              value={text}
              required
              onChange={event => setText(event.target.value)}
            />
          </div>
          <div className="col-auto">
            <button type="submit" className="btn btn-primary">
              Search
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
