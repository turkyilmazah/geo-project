import React, {useEffect, useRef, useState} from 'react';
// @ts-ignore
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
import {OfficeResponse} from '../types/OfficeResponse';

export function Map(props: {offices: OfficeResponse[]}) {
  const mapRef = useRef(null);
  const [map, setMap] = useState<mapboxgl.Map>();

  useEffect(() => {
    mapboxgl.accessToken =
      'pk.eyJ1IjoiYWhtZXR0dXJreWlsbWF6IiwiYSI6ImNrdzZyNGIzdzB0N3Ayb2xqZ3dxc3k0czQifQ.X-cuanxvxvl2E6ojfV0c2A';

    const map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-0.142571, 51.5144636],
      zoom: 8,
    });
    console.log('2', map);

    map.on('load', () => {
      map.addSource('places', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          // @ts-ignore
          features: companyInfo(),
        },
      });

      map.addLayer({
        id: 'places',
        type: 'circle',
        source: 'places',
        paint: {
          'circle-color': '#4264fb',
          'circle-radius': 6,
          'circle-stroke-width': 2,
          'circle-stroke-color': '#ffffff',
        },
      });
      map.resize();

      const popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false,
      });

      map.on('mouseenter', 'places', (e: any) => {
        map.getCanvas().style.cursor = 'pointer';

        // @ts-ignore
        const coordinates = e.features[0].geometry.coordinates.slice();
        // @ts-ignore
        const description = e.features[0].properties.description;
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        popup.setLngLat(coordinates).setHTML(description).addTo(map);
      });

      map.on('mouseleave', 'places', () => {
        map.getCanvas().style.cursor = '';
        popup.remove();
      });
      setMap(map);
    });
    const marker1 = new mapboxgl.Marker().setLngLat([-0.142571, 51.5144636]).addTo(map);
    return () => map.remove();
  }, [mapRef]);

  useEffect(() => {
    addData();
  }, [props.offices]);

  const addData = () => {
    if (map) {
      // @ts-ignore
      map.getSource('places').setData({
        type: 'FeatureCollection',
        // @ts-ignore
        features: companyInfo(),
      });
    }
  };

  const companyInfo = () => {
    console.log('offices', props.offices);
    return props.offices.map(item => {
      return {
        type: 'Feature',
        properties: {
          description: `<strong>${item.organization}<strong><br/><small>${item.distance}</small><br/><p>${item.services}</p>`,
        },
        geometry: {
          type: 'Point',
          coordinates: item.coordinates,
        },
      };
    });
  };

  return <div className="map-content" ref={mapRef} id="map" />;
}
