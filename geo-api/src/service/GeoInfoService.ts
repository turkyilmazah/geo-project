import {geoInfoRepository} from "../repository/GeoInfoRepository";
import {OfficeResponse} from "../types/OfficeResponse";

class GeoInfoService {

  getAll = async () => {
    return await geoInfoRepository.getAll();
  }

  findGreatCircleOfDistance = async (lat: number, lng: number, distance: number) => {
    return await geoInfoRepository.getAllResponses(lat, lng, distance);
  }

  findSortedDistance = async (lat: number, lng: number, distance: number) => {
    const officeResponses: OfficeResponse[] = await this.findGreatCircleOfDistance(lat, lng, distance)
    return officeResponses.sort((a, b) => a.organization > b.organization ? 1 : -1);
  }

}


export const geoInfoService = new GeoInfoService();
