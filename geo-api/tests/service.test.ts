import {geoInfoService} from "../src/service/GeoInfoService";
import {sampleGeoInfo, sampleSortedResponse} from "./sampleData";
import arrayContaining = jasmine.arrayContaining;

describe('geoService', () => {
  let lat: any;
  let lng: any;
  let distance: any;

  beforeAll(async () => {
    lat = 51.5144636;
    lng = -0.142571;
    distance = 200;
  })
  describe(('get'), () => {
    test('getAll', async () => {
      return expect(geoInfoService.getAll())
        .resolves
        .toEqual(expect.arrayContaining(sampleGeoInfo));
    });
    test('getResponsesSorted', async () => {
      return expect(geoInfoService.findSortedDistance(lat, lng, distance)).resolves.toEqual(sampleSortedResponse);
    });
    test('getResponses', async () => {
      return expect(geoInfoService.findGreatCircleOfDistance(lat, lng, distance)).resolves.toEqual(arrayContaining(sampleSortedResponse));
    });
  });
  describe(('testCircle'), () => {

  });
});