import supertest from 'supertest'

const app = require('../server')

describe('office endpoint', () => {
  let lat: any;
  let lng: any;
  let distance: any;

  beforeAll(async () => {
    lat = 51.5144636;
    lng = -0.142571;
    distance = 200;
  })
  describe('positive test', () => {

    test('get all info', async () => {
      const res = await supertest(app).get('/api/geo-info');

      expect(res.statusCode).toEqual(200);
      expect(res.body.length).toStrictEqual(17);
      expect(res.body[1]).toHaveProperty('id');
    })

    test('get a response', async () => {
      const res = await supertest(app).get(`/api/office-info/${lat}/${lng}/${distance}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body[0]).toHaveProperty('distance');
    })

  })

  describe('negative test', () => {
    test('get a response - gives error if latitude is invalid', async () => {
      const res = await supertest(app).get(`/api/office-info/a3s2d/${lng}/${distance}`);

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error');
      expect(res.body.name).toEqual('InvalidCoordinatesException');
    })
    test('get a response - gives error if longitude is invalid', async () => {
      const res = await supertest(app).get(`/api/office-info/${lat}/a12a/${distance}`);

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error');
      expect(res.body.name).toEqual('InvalidCoordinatesException');
    })
    test('get a response - gives error if distance is invalid', async () => {
      const res = await supertest(app).get(`/api/office-info/${lat}/${lng}/as123}`);

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error');
      expect(res.body.name).toEqual('InvalidCoordinatesException');
    })
  })
})