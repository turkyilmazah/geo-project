# Geo-Project

This is a project that finds the distance between the given location and the location in the sampled database. 

### System Requirements

|Package|Version|
|---|---|
|Docker|19.03 +|
|Docker Engine|1.40 +|
|NodeJS|14.x +|

### Setup

**Run**

There are two running options as Docker and local.

Run the following command in the main directory to run this project with Docker.

```
docker-compose up -d
```
Run the following commands in order in the main directory of the project to run this project in local.

```
cd geo-api
npm install
npm start
cd ../geo-ui/
npm install
npm start
```
**Test**

Run this command in both directories of the project to test.

```
npm test
```

## Geo-Api

Backend API is made with ExpressJS. With the usage of Layered Architecture there are 4 main 
layers.

**Router Layer**

Contains the API routes of the app. Its only job is to return a response from the server.
For getting all the info in the db, the route is `/geo-info`. For getting all the response by the given distance info is
`/office-info/:lat/:lng/:distance`.

**Controller**

Gets the request from the router, validates the request by checking the path variables, request parameters, headers, and sends it to the service layer.

**Service**

This is the business layer that we implement our business logic. We get the response object from the repository layer. And sort them by their organization Name.

**Repository**

This is the layer where we access the DB and get the office locations and get the distance between the given coordinate and the coordinates in the DB.
If the distance is smaller than the given distance in the request, we add it to the response object with a distance field.
We could have handled this part in a DB query and get a faster response. Since we did not use a DB we handled it in the repository layer.
And hand over the response object to the service layer. 

Calculations that done in this project to calculate the Great Circle Distance is Haversine Formula's shortened version calculated with the half angle formulas. 
The reason why I used the Haversine formula is although the rounding errors it is the most common one.

**Tests**

Service and route tests are done in this project. Positive tests done for getting the wanted value and the status of 200.
 Negative Tests are done for getting the errors and the unexpected values.

## Geo-UI

Front End application is made with ReactJS. For the map, MapBox is used in this project.
To send the request to the server Axios library is used. Bootstrap is used for the css styling.

**Tests**

Components Card and CardList tested for their functionality. React Testing Library and jest used for testing.

## Docker

Docker is used to deploy the both server and the client. In the Dockerfile of the UI, there are 2 stages. In the first stage everything is usual like an ordinary node
app image except an argument passed for the definition of the server URL. In the second phase, the Nginx image is built and exposed to port 80.

Dockerfile of the server application is a simple NodeJS application Dockerfile.

In order to make a Continuous Delivery Workspace and connect the two containers
and allow them to communicate with each other by creating a default network. A docker-compose 
file is made and both geo-ui and geo-api are added. 
